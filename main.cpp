#include "SR05_Act04.hpp"

int main(void) {
    SR05_Act04 sr05_act04{};

    if(sr05_act04.start() == -1)
        std::exit(EXIT_FAILURE);

    std::exit(EXIT_SUCCESS);
}
