#include <cstdio>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include "SR05_Act04.hpp"

SR05_Act04::SR05_Act04() {
    cout << "Entrer le message à répéter : ";
    cin >> stdoutMsg;

    cout << "Entrer un interval entre chaque répétition (sec) : ";
    cin >> interval;
}

int SR05_Act04::start() {
    fd_set rfds;
    timeval tv;
    int retval;
    char buf;
    string stdinMsg = "";

    cout << "Ecrire 'stop' pour terminer ..." << endl;

    while(stdinMsg != "stop") {
        stdinMsg.clear();

        /* Surveiller stdin (fd 0) en attente d'entrées */
        FD_ZERO(&rfds);
        FD_SET(0, &rfds);

        /* Interval */
        tv.tv_sec = interval;
        tv.tv_usec = 0;

        retval = select(1, &rfds, NULL, NULL, &tv);
        if (retval == -1) {
            perror("select()");
            return -1;
        }
        else if (retval) {
            read(0, &buf, 1);
            while(buf != '\n') {
                stdinMsg += buf;
                read(0, &buf, 1);
            }
            cerr << "Message recus (stdin) : " << stdinMsg << endl;
        }
        else {
            cout << stdoutMsg << endl;
        }
    }
    return 0;
}
